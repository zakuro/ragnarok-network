usage() {
  cat << EOS
sh install.sh [device] [number]
  device: Network device name
  number: Host number in 2 to 255
EOS
}


device=$1
number=$2

if [ -z "$device" -o -z "$number" ]
then
  usage
  exit 1
fi

cat ./ragnarok.network \
  | sed "s/%DEVICE%/$device/" \
  | sed "s/%NUMBER%/$number/" \
  > /etc/systemd/network/ragnarok.network
